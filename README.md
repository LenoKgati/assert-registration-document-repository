# assert-registration-document-repository

# register-assets-service
Asset Management Backend
# Overview
The Asset Management Backend is a Spring Boot application designed to handle the server-side logic for an Asset Management System (AMS). It provides RESTful APIs for managing assets, ensuring authentication and authorization, and maintaining audit trails.

# Technologies Used
Java 17
Spring Boot
Spring Security
Maven
MySQL
Apache Kafka
Elasticsearch
Docker
Kubernetes
# Getting Started
Prerequisites
Java 17
Maven
MySQL Server
Docker (optional for containerization)
Kubernetes (optional for orchestration)
# Installation
Clone the repository: - https://gitlab.com/LenoKgati/assert-registration-document-repository.git
- cd asset-management-backend
# Configure MySQL Database:
spring.application.name=documents-repositories-service
server.port=8082
spring.datasource.url=jdbc:mysql://localhost:3306/document_repo
spring.datasource.username=root
spring.datasource.password=
spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQLDialect


# File storage location
file.storage.location=document_repo_files
spring.servlet.multipart.max-file-size=10MB
spring.servlet.multipart.max-request-size=10MB

# Build Project
mvn clean install

# Run
mvn spring-boot:run
